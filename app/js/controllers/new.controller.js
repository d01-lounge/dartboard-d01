(function(ng) {
    'use strict';

    ng.module('dartboard')
        .controller('controller.new', [

            '$state',
            'service.game',

            function GameController($state, GameService) {


                var vm = this;

                vm.players = [
                    {
                        'name': 'Mikey',
                        'nickname': 'Bear Claw',
                        'rounds': [],
                        'totalGame': 0
                    },
                    {
                        'name': 'Laurens',
                        'nickname': 'Goat Master',
                        'rounds': [],
                        'totalGame': 0
                    },
                    {
                        'name': 'Mante',
                        'nickname': 'The Analyst',
                        'rounds': [],
                        'totalGame': 0
                    },
                    {
                        'name': 'Sander',
                        'nickname': 'Raging Beard',
                        'rounds': [],
                        'totalGame': 0
                    }
                ];

                ///////

                vm.startGame = function startGame() {
                    GameService.addPlayers(vm.players)
                        .then(function onPlayersAdded() {
                            $state.go('app.game');
                        }, function onAddPlayersFailed() {
                            console.error('No players found. Please retry!');
                            $state.go('app.new');
                        });
                };

            }


        ]);

})(angular);
