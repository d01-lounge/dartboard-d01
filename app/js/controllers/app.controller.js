(function(ng) {
    'use strict';

    ng.module('dartboard')
        .controller('controller.app', [

            '$state',

            function AppController($state) {

                var vm = this;

                vm.hello = "hello";
                vm.state = $state;

                vm.menu = [
                    { 'name': 'Home', 'state': 'app.home' },
                    { 'name': 'New game', 'state': 'app.new' },
                    { 'name': 'About', 'state': 'app.about' }
                ];

            }


        ]);

})(angular);
