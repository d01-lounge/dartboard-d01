(function(ng) {
    'use strict';

    ng.module('dartboard')
        .controller('controller.game', [

            '$state',
            'service.game',

            function GameController($state, GameService) {


                var vm = this;

                vm.activePlayer = 0;
                vm.throw = 0;
                vm.turn = {
                    'throws': [
                        { 'score': '*' },
                        { 'score': '*' },
                        { 'score': '*' }
                    ]
                };

                vm.totalHits = 0;

                /////// Scope Methods

                vm.nextPlayer = nextPlayer;

                /////// Functions

                initialize();

                function throwDart(e) {

                    if (vm.throw === 3) {
                        vm.throw = 0;
                        vm.reset = true;
                        vm.totalHits = 0;
                    }

                    GameService.calculateThrow({
                        'area': e.currentTarget.id,
                        'value': e.target.id
                    }).then(function onCalculatedThrow(t) {
                        vm.turn.throws[vm.throw] = t;
                        vm.totalHits += t.score;
                        vm.throw = vm.throw === 3 && vm.reset === true ? 0 : vm.throw += 1;
                        vm.reset = false;
                    });
                }

                function bindClickEvents() {
                    // A NodeList is not a real Array (with its functions -> forEach, ...);
                    // Array.prototype.slice.call() results in an Array;
                    var nodesArray = Array.prototype.slice.call(document.querySelectorAll('#dartboard #areas g'));
                    nodesArray.forEach(function (area) {
                        area.addEventListener('click', throwDart);
                    });
                }

                function checkPlayers() {
                    if (!GameService.players.length) {
                        console.error('No players found. Please add some players!');
                        return $state.go('app.new');
                    }
                    vm.players = GameService.players;
                    console.log('vm.players', vm.players);
                }

                function resetTurnValues() {
                    vm.throw = 0;
                    vm.totalHits = 0;
                    vm.turn = {
                        'throws': [
                            { 'score': '*' },
                            { 'score': '*' },
                            { 'score': '*' }
                        ]
                    };
                    console.info('resetted turn');
                }

                function nextPlayer() {
                    if (vm.throw !== 3) {
                        return;
                    }
                    // Put all throws of current turn, to rounds Array of player.
                    var currentPlayer = vm.players[vm.activePlayer];
                    // currentPlayer.rounds.push(vm.turn.throws);
                    var totalRound = 0;
                    _.each(vm.turn.throws, function (t) {
                        totalRound += t.score;
                        currentPlayer.totalGame += t.score;
                    });
                    currentPlayer.rounds.push({
                        'totalRound': totalRound,
                        'throws': vm.turn.throws
                    });

                    // Set next player to active player.
                    vm.activePlayer = vm.activePlayer === (vm.players.length - 1) ? 0 : vm.activePlayer += 1;

                    resetTurnValues();
                }

                function initialize() {
                    checkPlayers();
                    bindClickEvents();
                }

            }


        ]);

})(angular);
