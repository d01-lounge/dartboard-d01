(function dartboardModule(ng) {
    'use strict';

    var dartboard = ng.module('dartboard', ['ui.router']);

    dartboard.config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app', {
                abstract: true,
                redirectTo: 'app.home',
                templateUrl: 'views/app.htm',
                controller: 'controller.app',
                controllerAs: 'app'
            })
            .state('app.home', {
                url: '^/home',
                templateUrl: 'views/app.home.htm'
            })
            .state('app.new', {
                url: '^/new',
                templateUrl: 'views/app.new.htm',
                controller: 'controller.new',
                controllerAs: 'new'
            })
            .state('app.game', {
                url: '^/game',
                templateUrl: 'views/app.game.htm',
                controller: 'controller.game',
                controllerAs: 'game'
            })
            .state('app.about', {
                url: '^/about',
                templateUrl: 'views/app.about.htm'
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/home');


    });

})(angular);
