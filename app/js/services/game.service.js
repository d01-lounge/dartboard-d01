(function(ng) {
    'use strict';

    ng.module('dartboard')
        .service('service.game', [

            '$q',

            function GameService($q) {

                var multiplier = {
                    "single": 1,
                    "double": 2,
                    "triple": 3,
                    "bull": 1,
                    "outer": 0
                };

                var service = {
                    calculateThrow: calculateThrow,
                    addPlayers: addPlayers,
                    players: []
                };

                return service;

                ///////

                function calculateThrow(dart) {

                    var obj = {};
                    if (dart.area === 'blank' || dart.area === 'frame') {
                        obj.score = 0;
                    } else {
                        obj.score = multiplier[dart.value.split('-')[0]] * parseInt(dart.value.split('-')[1], 10);
                    }
                    obj.area = dart.area;
                    obj.value = typeof dart.value.split('-')[1] === 'undefined' ? 0 : parseInt(dart.value.split('-')[1]);

                    if (typeof dart.value.split('-')[2] !== 'undefined') {
                        obj.section = dart.area + '-' + dart.value.split('-')[2];
                    } else {
                        obj.section = dart.area + '-ring';
                    }

                    return $q.when(obj);

                }

                function addPlayers(players) {
                    if (players.length) {
                        service.players = players;
                        return $q.when(players);
                    }
                }

            }


        ]);

})(angular);