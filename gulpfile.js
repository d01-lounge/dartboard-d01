var gulp = require('gulp');
var connect = require('gulp-connect'),
    sass = require('gulp-sass');

var paths = {
    html: ['./app/**/*.html', './app/**/*.htm']
};

gulp.task('connect', function() {
    connect.server({
        root: 'app',
        livereload: true,
        port: 8016
    });
});

gulp.task('html', function () {
    gulp.src(paths.html)
        .pipe(connect.reload());
});

gulp.task('sass', function () {
  return gulp.src('./scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'))
    .pipe(connect.reload());
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
});

gulp.task('watch', function () {
    gulp.watch(paths.html, ['html']);
});

gulp.task('default', ['connect', 'watch', 'sass:watch']);